// WARNING! There is a lot of dublicate 'cause i couldn't connect second player SOCKET, sorry !!!

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <WinSock2.h>

#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 8400

#pragma comment(lib, "ws2_32.lib")

class CTcpServer
{
private:
	SOCKET serverSock;
	SOCKET clientSock;
	SOCKADDR_IN serverData;
	std::string m_ipAddress;
	int m_port;
public:
	CTcpServer(std::string ipAddress, int port);
	~CTcpServer();
	// main run server method
	void runServer();
	// run server method, but separately
	void initialize();
	void createSocket();
	void bindSocket();
	void listenToSocket();
	void acceptConnection();
	// change port
	void changePort();
	// closing socket
	void closeSocket();
	void cleanup();
	// recv/send methods
	void recvFromClient();
	int recvPart();
	long recvLength();
	void sendAnswer(long length, const std::string &str);
};

CTcpServer::CTcpServer(std::string ipAddress, int port)
	: m_ipAddress(ipAddress), m_port(port), serverSock(INVALID_SOCKET), clientSock(INVALID_SOCKET), serverData({0})
{	}

CTcpServer::~CTcpServer()
{
	closesocket(serverSock);
	cleanup();
}

void CTcpServer::initialize()
{
	WSAData data;
	if (WSAStartup(MAKEWORD(2, 2), &data) < 0) {
		std::cout << "Failed to init sock lib: " << WSAGetLastError() << std::endl;
		exit(1);
	}
}

void CTcpServer::createSocket()
{
	serverSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (serverSock == INVALID_SOCKET) {
		std::cout << "Failed to create a server socket: " << WSAGetLastError() << std::endl;
		exit(1);
	}
}

void CTcpServer::bindSocket()
{
	serverData.sin_family = AF_INET;
	serverData.sin_port = htons(m_port);
	serverData.sin_addr.s_addr = INADDR_ANY;

	if (bind(serverSock, (sockaddr*)&serverData, sizeof(serverData)) < 0)
	{
		std::cout << "Failed to bind socket " << WSAGetLastError() << std::endl;
		exit(1);
	}
}

void CTcpServer::listenToSocket()
{
	listen(serverSock, 5);
}

void CTcpServer::acceptConnection()
{
	clientSock = accept(serverSock, NULL, NULL);
	if (clientSock == INVALID_SOCKET) {
		std::cout << "Faield to accept " << WSAGetLastError() << std::endl;
		exit(1);
	}
}

void CTcpServer::recvFromClient()
{
	char buf[1024] = {};
	std::string str;
	do
	{
		memset(buf, 0, sizeof(buf));
		recv(clientSock, buf, 1024, 0);
		str = buf;
	}while (str != "STOP");
	
	std::cout << buf << std::endl;
}

void CTcpServer::closeSocket()
{
	closesocket(serverSock);
}

void CTcpServer::cleanup()
{
	WSACleanup();
}

void CTcpServer::changePort()
{
	m_port += 5;
	closeSocket();
}

int CTcpServer::recvPart()
{
	char buf[4096];
	std::string str;
	std::string part;
	int bytesReceived = 0;
	long length = 0;
	do
	{
		memset(buf, 0, sizeof(buf));
		length = recvLength();
		if (length == -1)
		{
			std::cout << "ERROR DURING FILE SENDING!" << std::endl;
			return -1;
		}
		recv(clientSock, buf, sizeof(buf), 0);
		sendAnswer(length, buf);
		part.append(buf);
		str = buf;
	}
	while (str != "END");

	// open file and append part
	std::ofstream f;
	f.open("test.lsv", std::ios::binary, std::ios_base::app);
	f << part;
	f.close();

	static int i = 20;
	std::cout << "SENDED " << i << "% OF THE FILE - " << std::endl;
	i += 20;

	return 1;
}

long CTcpServer::recvLength()
{
	char buf[256] = {};
	recv(clientSock, buf, sizeof(buf), 0);
	std::string str = buf;
	if (str == "BREAK")
	{
		return -1;
	}
	return std::stol(str);
}

void CTcpServer::sendAnswer(long length, const std::string &str)
{
	if (str.length() == length)
	{
		send(clientSock, "true", strlen("true"), 0);
	}
	else
	{
		send(clientSock, "false", strlen("false"), 0);
	}
}

void CTcpServer::runServer()
{
	// create socket
	initialize();
	createSocket();
	bindSocket();
	listenToSocket();
	acceptConnection();
}

int main()
{
	// create socket
	CTcpServer serverSock(SERVER_IP, SERVER_PORT);
	for (int i = 0; i < 5; ++i)
	{
		// open/reopen socket
		serverSock.runServer();
		// send fifth part of the file
		if (serverSock.recvPart() == -1)
		{
			return -1;
		}
		// close socket
		serverSock.cleanup();
		// change port +5
		serverSock.changePort();
	}
	return 0;
}